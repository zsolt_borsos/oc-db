-- triggers

delimiter |

CREATE TRIGGER addDefaultClientDetails AFTER INSERT ON client
  FOR EACH ROW
  BEGIN
    INSERT INTO clientDeliveryPoint (`name`, `note`, `clientId`, `default`) VALUES ('Main', 'default', NEW.id, 1);
    INSERT INTO clientDepartment (`name`, `clientId`) VALUES ('Main', NEW.id);
  END;
|



delimiter ;

-- triggers

delimiter |

CREATE TRIGGER addPriceFromSalesData AFTER INSERT ON purchaseItem
  FOR EACH ROW
  BEGIN
  SET @calculatedPrice  := NEW.total / NEW.qty;

  SELECT date INTO @d1 FROM purchase WHERE id = NEW.purchaseId;
    INSERT INTO price (`productId`, `dateFrom`, `value`, `sourceTypeId`, `sourceValue`, `note` ) VALUES (NEW.productId, @d1, @calculatedPrice, 1, 'SalesData', 'From Sales Data');
  END;
|



delimiter ;
