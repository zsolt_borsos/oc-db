# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.23)
# Database: onecost
# Generation Time: 2015-04-30 02:03:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accountManager
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accountManager`;

CREATE TABLE `accountManager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `contact` varchar(250) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accountManager_supplier` (`supplierId`),
  CONSTRAINT `accountManager_supplier` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table associate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `associate`;

CREATE TABLE `associate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `associateTypeId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `associates_associateType` (`associateTypeId`),
  CONSTRAINT `associates_associateType` FOREIGN KEY (`associateTypeId`) REFERENCES `associateType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table associateReferredBy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `associateReferredBy`;

CREATE TABLE `associateReferredBy` (
  `associateId` int(11) NOT NULL,
  `referredById` int(11) NOT NULL,
  `value` decimal(7,4) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`associateId`,`referredById`),
  KEY `referredBy_associate2` (`referredById`),
  CONSTRAINT `referredBy_associate` FOREIGN KEY (`associateId`) REFERENCES `associate` (`id`),
  CONSTRAINT `referredBy_associate2` FOREIGN KEY (`referredById`) REFERENCES `associate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table associateType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `associateType`;

CREATE TABLE `associateType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `payable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc`;

CREATE TABLE `cc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `note` text,
  `clientId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cc_client` (`clientId`),
  KEY `cc_user` (`userId`),
  CONSTRAINT `cc_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  CONSTRAINT `cc_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `levelOfInterest` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) DEFAULT NULL,
  `clientGroupId` int(11) DEFAULT NULL,
  `industryId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_area` (`locationId`),
  KEY `client_clientGroup` (`clientGroupId`),
  KEY `client_industry` (`industryId`),
  CONSTRAINT `client_area` FOREIGN KEY (`locationId`) REFERENCES `location` (`id`),
  CONSTRAINT `client_clientGroup` FOREIGN KEY (`clientGroupId`) REFERENCES `clientGroup` (`id`),
  CONSTRAINT `client_industry` FOREIGN KEY (`industryId`) REFERENCES `industry` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `addDefaultClientDetails` AFTER INSERT ON `client` FOR EACH ROW BEGIN
    INSERT INTO clientDeliveryPoint (`name`, `note`, `clientId`, `default`) VALUES ('Main', 'default', NEW.id, 1);
    INSERT INTO clientDepartment (`name`, `clientId`) VALUES ('Main', NEW.id);
  END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table clientAccountManager
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientAccountManager`;

CREATE TABLE `clientAccountManager` (
  `clientId` int(11) NOT NULL,
  `accountManagerId` int(11) NOT NULL,
  PRIMARY KEY (`clientId`,`accountManagerId`),
  KEY `clientManagerLink_accountManager` (`accountManagerId`),
  CONSTRAINT `clientManagerLink_accountManager` FOREIGN KEY (`accountManagerId`) REFERENCES `accountManager` (`id`),
  CONSTRAINT `clientManagerLink_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientDeliveryPoint
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientDeliveryPoint`;

CREATE TABLE `clientDeliveryPoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `default` tinyint(1) NOT NULL,
  `clientId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientDeliveryPoint_client` (`clientId`),
  CONSTRAINT `clientDeliveryPoint_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientDepartment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientDepartment`;

CREATE TABLE `clientDepartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `clientId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientDepartment_client` (`clientId`),
  CONSTRAINT `clientDepartment_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientGroup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientGroup`;

CREATE TABLE `clientGroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientReferral
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientReferral`;

CREATE TABLE `clientReferral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(7,4) DEFAULT NULL,
  `clientId` int(11) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `associateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientReferral_associate` (`associateId`),
  KEY `referral_client` (`clientId`),
  CONSTRAINT `clientReferral_associate` FOREIGN KEY (`associateId`) REFERENCES `associate` (`id`),
  CONSTRAINT `referral_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientStrategicArea
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientStrategicArea`;

CREATE TABLE `clientStrategicArea` (
  `strategicAreaId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  PRIMARY KEY (`strategicAreaId`,`clientId`),
  KEY `strategicAreaLink_client` (`clientId`),
  CONSTRAINT `strategicAreaLink_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  CONSTRAINT `strategicAreaLink_strategicArea` FOREIGN KEY (`strategicAreaId`) REFERENCES `strategicArea` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clientSubdivision
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientSubdivision`;

CREATE TABLE `clientSubdivision` (
  `supplierId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  `subdivisionId` int(11) NOT NULL,
  PRIMARY KEY (`supplierId`,`clientId`),
  KEY `clientInSubdivisionWithSupplier_subdivision` (`subdivisionId`),
  KEY `supplierClientDetails_client` (`clientId`),
  CONSTRAINT `clientInSubdivisionWithSupplier_subdivision` FOREIGN KEY (`subdivisionId`) REFERENCES `subdivision` (`id`),
  CONSTRAINT `clientInSubdivisionWithSupplier_supplier` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`id`),
  CONSTRAINT `supplierClientDetails_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table county
# ------------------------------------------------------------

DROP TABLE IF EXISTS `county`;

CREATE TABLE `county` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table departmentAccountDetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departmentAccountDetails`;

CREATE TABLE `departmentAccountDetails` (
  `clientDepartmentId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `accountNumber` varchar(50) NOT NULL,
  `accountName` varchar(250) NOT NULL,
  PRIMARY KEY (`clientDepartmentId`,`supplierId`),
  KEY `accountNumber_supplier` (`supplierId`),
  CONSTRAINT `accountNumber_clientDepartment` FOREIGN KEY (`clientDepartmentId`) REFERENCES `clientDepartment` (`id`),
  CONSTRAINT `accountNumber_supplier` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table departmentDeliveryPoint
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departmentDeliveryPoint`;

CREATE TABLE `departmentDeliveryPoint` (
  `clientDepartmentId` int(11) NOT NULL,
  `clientDeliveryPointId` int(11) NOT NULL,
  PRIMARY KEY (`clientDepartmentId`,`clientDeliveryPointId`),
  KEY `departmentDeliveryPoint_clientDeliveryPoint` (`clientDeliveryPointId`),
  CONSTRAINT `departmentDeliveryPoint_clientDeliveryPoint` FOREIGN KEY (`clientDeliveryPointId`) REFERENCES `clientDeliveryPoint` (`id`) ON DELETE CASCADE,
  CONSTRAINT `departmentDeliveryPoint_clientDepartment` FOREIGN KEY (`clientDepartmentId`) REFERENCES `clientDepartment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table industry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `industry`;

CREATE TABLE `industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table internalAccountManager
# ------------------------------------------------------------

DROP TABLE IF EXISTS `internalAccountManager`;

CREATE TABLE `internalAccountManager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `internalAccountManager_client` (`clientId`),
  KEY `internalAccountManager_user` (`userId`),
  CONSTRAINT `internalAccountManager_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  CONSTRAINT `internalAccountManager_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `ccId` int(11) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `done` tinyint(1) DEFAULT NULL,
  `supplierId` int(11) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_cc` (`ccId`),
  CONSTRAINT `invoice_cc` FOREIGN KEY (`ccId`) REFERENCES `cc` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoiceItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoiceItem`;

CREATE TABLE `invoiceItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` decimal(12,2) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoiceItem_invoice` (`invoiceId`),
  KEY `invoiceItem_product` (`productId`),
  CONSTRAINT `invoiceItem_invoice` FOREIGN KEY (`invoiceId`) REFERENCES `invoice` (`id`),
  CONSTRAINT `invoiceItem_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table location
# ------------------------------------------------------------

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `countyId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_county` (`countyId`),
  CONSTRAINT `location_county` FOREIGN KEY (`countyId`) REFERENCES `county` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table measureType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `measureType`;

CREATE TABLE `measureType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `price`;

CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `value` decimal(12,2) NOT NULL,
  `sourceTypeId` int(11) NOT NULL,
  `sourceValue` varchar(250) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `price_product` (`productId`),
  KEY `price_sourceType` (`sourceTypeId`),
  CONSTRAINT `price_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `price_sourceType` FOREIGN KEY (`sourceTypeId`) REFERENCES `sourceType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productCode` varchar(150) NOT NULL DEFAULT '',
  `productCategoryId` int(11) DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `unitOfSale` decimal(12,2) DEFAULT NULL,
  `measureValue` decimal(12,4) DEFAULT NULL,
  `measureTypeId` int(11) DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_measureType` (`measureTypeId`),
  KEY `product_productCategory` (`productCategoryId`),
  KEY `product_supplier` (`supplierId`),
  CONSTRAINT `product_measureType` FOREIGN KEY (`measureTypeId`) REFERENCES `measureType` (`id`),
  CONSTRAINT `product_productCategory` FOREIGN KEY (`productCategoryId`) REFERENCES `productCategory` (`id`),
  CONSTRAINT `product_supplier` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productCategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productCategory`;

CREATE TABLE `productCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `productCategoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productCategory_productCategory` (`productCategoryId`),
  CONSTRAINT `productCategory_productCategory` FOREIGN KEY (`productCategoryId`) REFERENCES `productCategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productDetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productDetails`;

CREATE TABLE `productDetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `packsize` varchar(255) DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `productDetails_product` (`productId`),
  CONSTRAINT `productDetails_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productMatch
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productMatch`;

CREATE TABLE `productMatch` (
  `invoiceItemId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `priceId` int(11) NOT NULL,
  `qty` decimal(12,2) NOT NULL,
  `final` tinyint(1) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`invoiceItemId`,`productId`),
  KEY `productMatch_price` (`priceId`),
  KEY `productMatch_product` (`productId`),
  CONSTRAINT `productMatch_invoiceItem` FOREIGN KEY (`invoiceItemId`) REFERENCES `invoiceItem` (`id`),
  CONSTRAINT `productMatch_price` FOREIGN KEY (`priceId`) REFERENCES `price` (`id`),
  CONSTRAINT `productMatch_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table purchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase`;

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `note` text,
  `clientDepartmentId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_clientDepartment` (`clientDepartmentId`),
  CONSTRAINT `purchase_clientDepartment` FOREIGN KEY (`clientDepartmentId`) REFERENCES `clientDepartment` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table purchaseItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchaseItem`;

CREATE TABLE `purchaseItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchaseId` int(11) NOT NULL,
  `qty` decimal(12,4) NOT NULL,
  `total` decimal(12,2) NOT NULL,
  `productId` int(11) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_item_product` (`productId`),
  KEY `purchase_purchase_item` (`purchaseId`),
  CONSTRAINT `purchase_item_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `purchase_purchase_item` FOREIGN KEY (`purchaseId`) REFERENCES `purchase` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `addPriceFromSalesData` AFTER INSERT ON `purchaseItem` FOR EACH ROW BEGIN
  SET @calculatedPrice  := NEW.total / NEW.qty;

  SELECT date INTO @d1 FROM purchase WHERE purchase.id = NEW.purchaseId;
    INSERT INTO price (`productId`, `dateFrom`, `value`, `sourceTypeId`, `sourceValue`, `note` ) VALUES (NEW.productId, @d1, @calculatedPrice, 1, 'SalesData', 'From Purchase Item Trigger');
  END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table rebate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rebate`;

CREATE TABLE `rebate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `value` decimal(7,3) NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rebate_client` (`clientId`),
  KEY `rebate_product` (`productId`),
  CONSTRAINT `rebate_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  CONSTRAINT `rebate_product` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sourceType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sourceType`;

CREATE TABLE `sourceType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table strategicArea
# ------------------------------------------------------------

DROP TABLE IF EXISTS `strategicArea`;

CREATE TABLE `strategicArea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table subdivision
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subdivision`;

CREATE TABLE `subdivision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `note` text,
  `defaultRebate` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supplierArea
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplierArea`;

CREATE TABLE `supplierArea` (
  `locationId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`locationId`,`supplierId`),
  KEY `supplierAreaLink_supplier` (`supplierId`),
  CONSTRAINT `supplierAreaLink_area` FOREIGN KEY (`locationId`) REFERENCES `location` (`id`),
  CONSTRAINT `supplierAreaLink_supplier` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(150) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `accessLevel` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
